jQuery(document).ready(function() {
	var editor = ace.edit("editor");
	editor.setTheme("ace/theme/eclipse");	    
    editor.getSession().setUseWrapMode(true);
    editor.setShowPrintMargin(false);
    editor.getSession().on('change', function(){
    	jQuery('#edit-orz-code-editor-edit-text').val(editor.getSession().getValue());
    
    });

});