jQuery(document).ready(function() {
	var editor = ace.edit("editor");
	editor.setTheme("ace/theme/eclipse");	    
	var ScriptMode = require("ace/mode/javascript").Mode;
    editor.getSession().setMode(new ScriptMode());
    editor.getSession().setUseWrapMode(true);
    editor.setShowPrintMargin(false);
});