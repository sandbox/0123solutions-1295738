
orz Development Toolbox: Code Editor

-directly view, edit, and save website's files from the website
-changes can be saved in database or committed to real files
-three levels of authority: view, edit, and commit for both themes and modules separately
-take the advantage of node's revision, Code Editor allow you to reload previous codes from both revisions and real files. 
-allow you link your own file extionsions to proper file types